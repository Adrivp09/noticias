import { Component } from '@angular/core';
import { NoticiasComponent } from 'src/app/components/noticias/noticias.component';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  categorias = ["BUSINESS", "ENTERTAINMENT", "GENERAL"]

  news: any = []
  numPage: number = 2
  categoria: string = "BUSINESS"


  constructor(private data: DataService, private noticias: NoticiasComponent) { }


  ngOnInit() {
    this.data.getCatego(1, this.categoria).toPromise().then((res: any) => {
      this.news = res.articles
    })
  }

  loadData(event) {

    setTimeout(() => {
      this.data.getCatego(this.numPage, this.categoria).toPromise().then((articulo: any) => {
        const info = articulo.articles
        info.forEach((element) => {
          this.news.push(element)
        });
        this.numPage++
      })
      console.log('done')
      event.target.complete();
    }, 2000);
  }

  ionViewWillEnter() {
    this.ngOnInit()
  }

  segmentChanged(e) {
    this.noticias.volverArriba()
    this.categoria = e.detail.value
    this.data.getCatego(1, e.detail.value).toPromise().then((res: any) => {
      this.news = res.articles
    })

  }

}
