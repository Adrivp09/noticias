import { Component, Input, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
import { DataLocalService } from 'src/app/services/data-local.service';

const { Browser } = Plugins;
const { Share } = Plugins;


@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.scss'],
})
export class NoticiasComponent implements OnInit {


  @Input('news') news: Array<any>
  @Input('loadData') loadData: any
  @Input('numPage') numPage: number
  @Input('tab') tab: string
  @Input('categoria') categoria: string



  constructor(public actionSheetController: ActionSheetController,private serviceFav:DataLocalService) {
  }
  ngOnInit() {
  }

  async abrirNavegador(url){
    await Browser.open({ url: url });
  }


  async lanzarMenu(noticia) {

    const actionSheet = await this.actionSheetController.create({
      animated:true,
      buttons: [
        {
        text: 'Compartir',
        icon: 'share',
      },
      {
        text: this.tab?'Borrar de favoritos':'Favoritos',
        role: this.tab?'borrar de favoritos':'favoritos',
        icon: 'star',
        handler:async () => {
          if(this.tab){
            const deleted= await this.serviceFav.delFav(noticia)
            this.news=deleted
          }else{
            console.log('Fav clicked');
            const addFav=await this.serviceFav.setFav(noticia)
          }
        }
      },
       {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancelar',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });

    await actionSheet.present();
  }

volverArriba() {
  getContent().scrollToTop();
}

}
function getContent() {
  return document.querySelector('ion-content');
}

