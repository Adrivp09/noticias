import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  favs: Array<any> = []

  constructor(private storage: Storage) {
    this.storage.create();
    this.cargarFav();
  }

  async delFav(fav){
    const newFavs=this.favs.filter(el=>fav.url!==el.url)
    this.favs=newFavs
    await this.storage.set('Favs', this.favs);
    return this.favs
  }

  async cargarFav() {
    const arr = await this.storage.get('Favs');
    this.favs = arr || []
    
  }
  async setFav(fav) {
    const filtrar =this.favs.filter(noti => fav.url === noti.url);
    if(filtrar[0]) return false
    this.favs.push(fav)
    await this.storage.set('Favs', this.favs);
    return true
  }

  

}
