import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment'; 

@Injectable({
  providedIn: 'root'
})
export class DataService {


  constructor(private http:HttpClient) { }

  getNoticias(page:number){
    const info= this.http.get(`https://newsapi.org/v2/top-headlines?language=es&page=${page}&apiKey=${environment.apiKey}`)
    return info
  }
  getCatego(page:number,category:string){
    const info= this.http.get(`https://newsapi.org/v2/top-headlines?language=es&page=${page}&category=${category}&apiKey=${environment.apiKey}`)
    return info
  }
}
